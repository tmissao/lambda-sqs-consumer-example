const config = require('../config/config');
const axios = require('axios');

const handler = async (event) => {

    const results =
        await Promise.all(
            event.Records.map((e) => {
                return axios.post(config.requestURL, JSON.parse(e.body));
            })
        )

    const values = results.map((e) => e.data);
    console.log(values);
    return values;
}

module.exports.handler = handler;