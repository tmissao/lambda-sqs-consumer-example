const lambda = require('./src/handler');

const event = {
    Records: [
        { body: JSON.stringify({ number: 1 }) },
        { body: JSON.stringify({ number: 2 }) },
        { body: JSON.stringify({ number: 3 }) },
        { body: JSON.stringify({ number: 4 }) },
        { body: JSON.stringify({ number: 5 }) },
        { body: JSON.stringify({ number: 6 }) },
        { body: JSON.stringify({ number: 7 }) },
        { body: JSON.stringify({ number: 8 }) },
        { body: JSON.stringify({ number: 9 }) },
        { body: JSON.stringify({ number: 0 }) }
    ]
}

lambda.handler(event);