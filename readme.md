# Lambda Request Example

The main intent of this project is to show an example of lambda that consumes messages ( 1 to 10 ) from AWS SQS Queue and performs external API Requests.

## Lambda Usage

To execute this lambda on AWS is necessary:

* To execute the npm install command
``` sh
    npm install --production
```

* Create a .zip file containing the folders: 
``` sh
    - node_modules/
    - config/
    - src/
```

* Upload this .zip file on AWS Lambda

* Set Lambda Handler path
``` sh
    src/handler.handler
```

* Define the following Role Polices:
``` javascript
    "sqs:DeleteMessage",      // Deletes message from SQS
    "sqs:ReceiveMessage",     // Receives message from SQS
    "sqs:SendMessage",        // Send message to SQS
    "sqs:GetQueueAttributes"  // Get Queue attributes

    // Cloud Watch Logs
    "logs:CreateLogGroup",
    "logs:CreateLogStream",
    "logs:PutLogEvents"
```

## Local Usage

To execute this lambda on localhost is necessary:

* To execute the npm install command
``` sh
    npm install --production
```

* Define the _**event**_ object argument at _**index.js**_

```javascript
const event = {
    Records: [
        { body: JSON.stringify({ number: 1 }) },
        { body: JSON.stringify({ number: 2 }) },
        { body: JSON.stringify({ number: 3 }) },
        { body: JSON.stringify({ number: 4 }) }
    ]
}
```

* Execute the npm start command
```
    npm start
```